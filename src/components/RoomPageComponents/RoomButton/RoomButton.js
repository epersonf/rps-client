import React from 'react';
import './RoomButton.css';

export default function RoomButton(props) {

    const joinRoom = (url) => {
        
    }

    return (
        <div className="room-button" onClick={joinRoom(props.id)}>
            <h3>{props.roomName}</h3>
            <p>{props.amountOfPlayers}/{props.maxAmountOfPlayers}</p>
        </div>
    )
}
