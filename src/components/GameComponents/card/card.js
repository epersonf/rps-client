import './Card.css';

//#region Import images
import VERSE from '../../../assets/VERSE.png';
import ROCK from '../../../assets/ROCK.png';
import PAPER from '../../../assets/PAPER.png';
import SCISSOR from '../../../assets/SCISSOR.png';
//#endregion

import React from 'react'

const images = [VERSE, ROCK, PAPER, SCISSOR];

export default function Card(props) {

    const clickCard = (index) => {
        //POST para servidor com informacoes
    }

    //#region Build Card Style
    let styleBuilder = "";
    if (props.isMyCard) styleBuilder += " my-card";
    //#endregion

    return (
        <div
        onClick={() => clickCard(props.index)}
        className={"card" + styleBuilder}
        style={
            {
                backgroundImage: "url(" + images[props.id] + ")",
                transform: "rotate(" + props.cardRotation + "deg)"
            }
        }
        >
            
        </div>
    )
}
