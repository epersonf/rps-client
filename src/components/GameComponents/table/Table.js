import React, {useContext} from 'react';
import './Table.css';
import Deck from '../deck/Deck';
import ShowCards from '../showCards/ShowCards';
import GameContext from '../../../pages/GamePage/GamePage';

export default function Table(props) {
    const context = useContext(GameContext);

    const actualizeCards = () => {
        
    }

    return (
        <div className="table">
            <Deck cardsRotation={-180}/>
            <div className="side-by-side">
                <Deck cardsRotation={90}/>
                <ShowCards />
                <Deck cardsRotation={-90}/>
            </div>
            <Deck isMyDeck={true}/>
        </div>
    );
}
