import './Deck.css';
import Card from '../card/Card';

import React, {useState} from 'react';

export default function Deck(props) {

    const [cardList, setCardList] = useState([]);
    
    return (
        <div className="deck">
            <Card id={0} isMyCard={props.isMyDeck} cardRotation={props.cardsRotation}/>
        </div>
    )
}
