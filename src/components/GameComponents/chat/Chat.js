import React, {useState} from 'react'
import './Chat.css';

export default function Chat(props) {
    const [input, setInput] = useState("");
    
    const sendMessage = (msg) => {
        if (msg === '') {
            //mostrar no chat mensagem dizendo para nao deixar em branco
            return;
        } 
        if (msg.startsWith('/')) {
            props.execute(msg);
        } else {
            //conectar com API e dar POST em mensagem
            //o comando abaixo deve ser colocado no then do POST
            props.addMessage(msg);
        }
        //clear em input
        setInput("");
    }

    return (
        <div className="chat">
            <textarea value={props.textarea} readOnly={true}></textarea>
            <div>
                <input value={input} type="text" onKeyDown={(e) => {if (e.keyCode === 13) sendMessage();}} onChange={(e) => setInput(e.target.value)}/>
                <button onClick={() => {sendMessage(input)}}>Send</button>
            </div>
        </div>
    );
}
