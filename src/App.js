import React, { createContext } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

//#region Pages import
import GamePage from './pages/GamePage/GamePage';
import LoginPage from './pages/LoginPage/LoginPage';
import RegisterPage from './pages/RegisterPage/RegisterPage';
import RoomsPage from './pages/RoomsPage/RoomsPage';
//#endregion

const LoginContext = createContext(null);

function App() {
  return (
    <div className="App">
      <LoginContext.Provider value={
        {
          "name": "",
          "email": "",
          "token": ""
        }
      }>
        <Router>
          <Switch>

            <Route path="/register">
              <RegisterPage />
            </Route>

            <Route path="/game">
              <GamePage />
            </Route>

            <Route path="/rooms">
              <RoomsPage />
            </Route>

            <Route path="/">
              <LoginPage />
            </Route>

          </Switch>
        </Router>
      </LoginContext.Provider>
    </div>
  );
}

export default App;
