import React, {useState} from 'react'
import Table from '../../components/GameComponents/table/Table';
import Chat from '../../components/GameComponents/chat/Chat';
import './GamePage.css';
import axios from 'axios';

const GameContext = React.createContext(undefined);

const developerMode = true;

let api = 
axios.create(
  {
      baseURL: 'https://localhost:3000/'
  }
);


export default function GamePage() {
    //#region Execute game commands
    const [textarea, setTextarea] = useState("Welcome to the chat. Type /help for help.")

    const addMessage = (msg) => {
        setTextarea(textarea + "\n" + msg);
    }

    const executeCommand = (cmd, fromChat=false) => {
        if (fromChat && !developerMode) return;
        const parameters = cmd.toLowerCase().split(' ');
        switch (parameters[0]) {
            case "/givecard":
                break;
            case "/removecard":
                break;
            case "/postcard":
                break;
            case "/clear":
                setTextarea("Chat cleared!");
                break;
            case "/help":
                setTextarea(textarea +
                    "\n\nCommands:" +
                "\n - /help -> Help." +
                "\n - /clear -> Clears chat." +
                "\n - /givecard [player (0 <-> 3)] [cardId (0 <-> 3)]" +
                "\n - /removecard [player (0 <-> 3)] [index]" +
                "\n - /postcard [position (-1 <-> 3)] [cardId (0 <-> 3)]" +
                "\n"
                );
                break;
            default:
                setTextarea(textarea + "\nCommand not found! Type /help for help.");
                break;
        }
    }
    //#endregion

    const [gameCards, setGameCards] = useState([[], [], [], [], []]);

    return (
        <section className="game-page">
            <GameContext.Provider value={
                {
                    'api': api
                }
            }>
                <div className="game-play">
                    <Table gameCards={gameCards}/>
                </div>
                <Chat execute={executeCommand} addMessage={addMessage} textarea={textarea} />
            </GameContext.Provider>
        </section>
    )
}
