import React from 'react'
import "./RoomsPage.css";
import RoomButton from '../../components/RoomPageComponents/RoomButton/RoomButton';

export default function RoomsPage() {

    

    return (
        <section className="room-page">
            <RoomButton id="" roomName="Sala" amountOfPlayers="3" maxAmountOfPlayers="4"/>
        </section>
    )
}
