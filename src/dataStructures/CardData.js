
//ownerIndex eh o index do player, pode ser de 0 -> 4, sendo 4 o id do ShowCards, que eh basicamente um deck tambem
//deckIndex eh o index da carta na mao do player, valor que sera passado como parametro para o servidor
//valueCode eh o codigo do background da carta, apenas para visualizacao, pois na pratica o servidor nao utiliza essa informacao, pois ele ja sabe

export default function buildCardData(ownerIndex, deckIndex, valueCode) {
    return {
        "ownerIndex": ownerIndex,
        "deckIndex": deckIndex,
        "valueCode": valueCode
    };
}